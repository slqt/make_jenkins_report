from django.urls import path

from . import api
from . import add_api
from . import views

urlpatterns = [
    path('', views.index,name = 'index'),
    path('dashboard', views.dashboard,name = 'dashboard'),
    path('cpjf', views.cpjf,name = 'cpjf'),
    # path('omp', views.omp,name = 'omp'),
    path('login', views.login,name = 'login'),


    path('get_cpjf', api.get_cpjf, name ='get_cpjf'),
    path('get_index', api.get_index, name ='get_index'),
    path('get_index_echarts', api.get_index_echarts, name='get_index_echarts'),
    path('get_dashboard_echarts', api.get_dashboard_echarts, name='get_dashboard_echarts'),
    path('get_dashboard', api.get_dashboard, name ='get_dashboard'),
    path('api_check', views.api_check, name ='api_check'),
    path('api_check1', views.api_check1, name='api_check1'),
    # path('service_status', views.service_status, name ='service_status'),

    path('test_api', add_api.test_api,name = 'test_api'),
    path('yapi_data', add_api.yapi_data,name = 'yapi_data'),


]