from django.http import HttpResponse, JsonResponse,HttpRequest
import jira
from jira import JIRA
import pymysql
import datetime
from . import config
import logging
import json


logger = logging.getLogger(__name__)


jira = JIRA(config.jira_url, basic_auth=(config.basic_user, config.basic_passwd))

def test_api(request):
    if request.method == 'POST':
        datas = dict(request.POST)
        print(datas)
    # print(request.META)
    # print (request.headers)
    datas=json.dumps(request)
    return JsonResponse(datas, safe=False, json_dumps_params={'ensure_ascii': False})

def swag_doc_to_jmx(request):
    from swaggerjmx.convert import conversion
    from swaggerjmx.settings import Settings as ST
    #  swagger_url
    if request.method != 'POST':
        return JsonResponse('只允许get请求', safe=False, json_dumps_params={'ensure_ascii': False})
    datas = request.body
    print(datas)
    print(request.META)
    ST.swagger_url = datas
    ST.report_path = 'jmx'
    # 开始转换
    conversion()
    #  report_path
    return JsonResponse(datas, safe=False, json_dumps_params={'ensure_ascii': False})

def yapi_data(request):
    if request.method == 'POST':
        datas = dict(request.POST)
        print(datas)
        host = "10.1.62.123"
        user = "root"
        password = "Auto_123"
        db = "jenkins"
        port = 3306
        connection = pymysql.connect(host=host, user=user,
                                 password=password, db=db, port=port, charset='UTF8')

    # print(request.META)
    # print (request.headers)
    datas=json.dumps(request)
    return JsonResponse(datas, safe=False, json_dumps_params={'ensure_ascii': False})

