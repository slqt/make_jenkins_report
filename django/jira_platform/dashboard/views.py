import pymysql
from django.shortcuts import render,redirect,reverse
from django.db import connection
import jira
from jira import JIRA
from . import config
from .get_issues import get_issues_today,get_tasks_today
import requests
import sys
import json


jira = JIRA(config.jira_url, basic_auth=(config.basic_user, config.basic_passwd))
connection = pymysql.connect(host=config.host, user=config.user,
                         password=config.password, db=config.db, port=config.port)


def index(request):
    get_issues_today()
    get_tasks_today()
    return render(request, 'index.html')


def dashboard(request):
    get_issues_today()
    get_tasks_today()
    return render(request, 'dashboard.html')


def cpjf(request):
    return render(request, 'cpjf.html')


def login(request):
    return render(request, 'login.html')
#
# def omp(request):
#     server_ip = '10.1.61.101'
#     port = 7500
#     username = 'admin'
#
#     password = 'Broada_123!'
#     headers = {'Content-Type': 'application/json'}
#
#     conn = requests.Session()
#     login_url = 'http://{}:{}/omp/api/v1/user/login'.format(server_ip, port)
#     login_data = {
#         'username': username,
#         'passwd': password
#     }
#     response = conn.post(
#         url=login_url,
#         json=login_data,
#         headers=headers
#     )
#     if response.status_code != 200:
#         print(
#             'Login failed reason:{}'.format(response.content.decode()))
#         sys.exit(1)
#
#     def get_datas(name=None):
#         # 2 是服务异常
#         # 3 是进程停止
#         # 1 是服务可用
#         status = {1: '服务可用', 3: '安装失败', 2: '服务异常', 0: '进程停止'}
#         url = 'http://{}:{}/omp/api/v1/installed_pkgs/query'.format(server_ip, port)
#         main_params = {'pageSize': 40}
#         if name:
#             main_params.update({'keyWord': name})
#         res = conn.get(url, headers=headers, params=main_params)
#         res = json.loads(res.text)
#         datas = []
#         for i in res['data']:
#             # print i
#             # print i['productName'],i['pkgName'],
#             for j in i['installedDiffInfoVOs']:
#                 datas.append((i['productName'], i['pkgName'], j['version'], status.get(j['status'], '未知'), j['ip']))
#                 # print i
#         return datas
#
#     datas = get_datas(name='platform')
#     return render(request, 'omp.html',service=datas)


def api_check1(request):
    if request.method == 'POST':
        datas = dict(request.META)
        print (datas)
        datas = dict(request.POST)
        print(datas)
    elif request.method == 'GET':
        datas = dict(request.GET)
        print (dict(datas))
        datas = dict(request.META)
        print(datas)
    return render(request, 'login.html')

def api_check(request):
    if request.method == 'POST':
        datas = dict(request.META)
        print(datas)
        datas = dict(request.POST)
        print(datas)
    elif request.method == 'GET':
        datas = dict(request.GET)
        print(dict(datas))
        datas = dict(request.META)
        print(datas)
    return render(request, 'login.html')
    # server_ip = '10.1.61.101'
    # port = 7500
    # username = 'admin'
    #
    # password = 'Broada_123!'
    # headers = {'Content-Type': 'application/json'}
    #
    # conn = requests.Session()
    # login_url = 'http://{}:{}/omp/api/v1/user/login'.format(server_ip, port)
    # login_data = {
    #     'username': username,
    #     'passwd': password
    # }
    # response = conn.post(
    #     url=login_url,
    #     json=login_data,
    #     headers=headers
    # )
    # if response.status_code != 200:
    #     print(
    #         'Login failed reason:{}'.format(response.content.decode()))
    #     sys.exit(1)
    #
    # def get_datas(type=1, name=None):
    #     # 2 是服务异常
    #     # 3 是进程停止
    #     # 1 是服务可用
    #     url = 'http://{}:{}/omp/api/v1/installed_pkgs/query'.format(server_ip, port)
    #     main_params = {'status': type, 'pageSize': 40}
    #     if name:
    #         main_params.update({'keyWord': name})
    #     res = conn.get(url, headers=headers, params=main_params)
    #     res = json.loads(res.text)
    #     for i in res['data']:
    #         print
    #         i['productName'], i['pkgName']
    #         for j in i['installedDiffInfoVOs']:
    #             print
    #             j['version']
    #             # print i
    #
    # get_datas(type=1, name='ant-')
    # return render(request, 'service.html')

