import jenkins
server = jenkins.Jenkins('http://jenkins.uyunsoft.cn:80', username="test", password="uyun_test")

jobs = server.get_jobs()
job_info={}
for job in jobs[0]['jobs']:
    job_info.update({job['fullname':]})

print (job_info)
def build(job_name,param_dict):
    global server
    server.build_job(job_name, parameters=param_dict)
    server.get_job_info(job_name)
    #print(server.jobs_count())

#
#
# 　　　#String参数化构建job名为job_name的job, 参数param_dict为字典形式，如：
#
# 　　　server.build_job(job_name, parameters=param_dict)
#
# 　　　#获取job名为job_name的job的相关信息
#
# 　　　server.get_job_info(job_name)
#
# 　　　#获取job名为job_name的job的最后次构建号
#
# 　　　server.get_job_info(job_name)['lastBuild']['number']
#
# 　　   #获取job名为job_name的job的某次构建的执行结果状态
#
# 　　　server.get_build_info(job_name,build_number)['result']　　
#
# 　　   #判断job名为job_name的job的某次构建是否还在构建中
#
# 　　　server.get_build_info(job_name,build_number)['building']