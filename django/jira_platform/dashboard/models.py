from django.db import models


class Issues(models.Model):
    class Meta:
        verbose_name_plural = '测试总单表'
    id=models.AutoField(primary_key = True)
    code = models.CharField(max_length=50,verbose_name='测试单编号',blank=True,db_index=True,unique=True)
    user = models.CharField(max_length=50, verbose_name='负责人', blank=True)
    reporter = models.CharField(max_length=50, verbose_name='测试负责人', blank=True)
    project = models.CharField(max_length=50, verbose_name='项目编码', blank=True)
    project_name= models.CharField(max_length=200, verbose_name='项目名称', blank=True)
    version= models.CharField(max_length=50, verbose_name='版本', blank=True)
    name = models.CharField(max_length=500, verbose_name='总单标题',blank=True)
    status = models.CharField(max_length=50, verbose_name='项目进度', blank=True)
    CHOICE_STATUS = (
        (1, u'是'),
        (0, u'否'),
    )
    active = models.IntegerField(default=1, choices=CHOICE_STATUS, verbose_name='是否参与统计', null=False)
    def __unicode__(self):
        return self.name


class Tasks(models.Model):
    class Meta:
        verbose_name_plural = 'BUG详细表'
    id=models.AutoField(primary_key = True)
    issue_key = models.CharField(max_length=50,verbose_name='测试总单编号',blank=True)
    task_key = models.CharField(max_length=50,verbose_name='任务编号',blank=True,db_index=True,unique=True)
    summary= models.CharField(max_length=200, verbose_name='bug概要', blank=True)
    day = models.DateField(auto_now_add=True, verbose_name='创建日期')
    status = models.CharField(max_length=50, verbose_name='状态', blank=True)
    type=models.CharField(max_length=50, verbose_name='类型', blank=True)
    priority=models.CharField(max_length=50, verbose_name='优先级', blank=True)
    reporter = models.CharField(max_length=50, verbose_name='报告人', blank=True)
    assignee = models.CharField(max_length=50, verbose_name='确认人', blank=True)
    bug_owner = models.CharField(max_length=50, verbose_name='bug制造人', blank=True)
    reopen = models.IntegerField(verbose_name='reopen次数', blank=True)
    def __unicode__(self):
        return self.name

class Reporter(models.Model):
    class Meta:
        verbose_name_plural = '测试人员表'
    id=models.AutoField(primary_key = True)
    name = models.CharField(max_length=50,verbose_name='报告人',blank=False,db_index=True)
    STATUS = (
        (1, u'是'),
        (0, u'否'),
    )
    active = models.IntegerField(default=1, choices=STATUS, verbose_name='是否参与统计', null=False)
    def __unicode__(self):
        return self.name


class Project(models.Model):
    class Meta:
        verbose_name_plural = '项目表'
    id=models.AutoField(primary_key = True)
    code = models.CharField(max_length=50, verbose_name='项目编号', blank=False,unique=True)
    name = models.CharField(max_length=50,verbose_name='项目总单名称',blank=False)
    summary=models.CharField(max_length=50,verbose_name='项目总单描述',blank=False)
    STATUS = (
        (1, u'是'),
        (0, u'否'),
    )
    active = models.IntegerField(default=1, choices=STATUS, verbose_name='是否参与统计', null=False)
    def __unicode__(self):
        return self.name


class Cpjf(models.Model):
    class Meta:
        verbose_name_plural = 'CPJF项目表'

    id = models.AutoField(primary_key=True)
    project=models.CharField(max_length=250, verbose_name='项目名称', blank=True,db_index=True,unique=True)
    doing = models.CharField(max_length=10, verbose_name='是否有未解决工单', blank=True)
    STATUS = (
        (1, u'是'),
        (0, u'否'),
    )
    active = models.IntegerField(default=1, choices=STATUS, verbose_name='是否参与统计', null=False)

    def __unicode__(self):
        return self.name


class jenkins(models.Model):
    class Meta:
        verbose_name_plural = 'jenkins流水线记录表'

    id = models.AutoField(primary_key=True)
    job_name=models.CharField(max_length=250, verbose_name='任务名称')
    desc = models.CharField(max_length=250, verbose_name='任务描述')
    version = models.CharField(max_length=20, verbose_name='版本')
    timestamp = models.CharField(max_length=10, verbose_name='时间戳')
    status = (
        (1, u'成功'),
        (0, u'失败'),
    )


    def __unicode__(self):
        return self.name
