# Register your models here.
from .models import Issues,Tasks,Reporter,Cpjf,Project
from django.contrib import admin


def get_readonly_fields(self, request, obj=None):
    """  重新定义此函数，限制普通用户所能修改的字段  """
    if request.user.is_superuser:
        self.readonly_fields = []
    return self.readonly_fields


class IssuesAdmin(admin.ModelAdmin):
    List_display_links = None
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(IssuesAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


    list_filter =('user',)
    readonly_fields = ('code','user', 'name','status')
    search_fields = ('code','name')
    list_display = ('code','user', 'name','status','active')
    list_editable = ['active']
    list_display_links = ('code', 'name')
    list_per_page = 20



class TasksAdmin(admin.ModelAdmin):
    List_display_links = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(TasksAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    list_filter =('day','priority','reporter','type')
    search_fields = ('issue_key','task_key','reporter','summary')
    list_display = ('issue_key','task_key', 'summary','day','status','priority','type','reporter','assignee')
    list_editable = []
    list_display_links = ('issue_key','task_key')
    readonly_fields = ('issue_key','task_key', 'day','status','priority','type','reporter','assignee')
    list_per_page = 20

class ReporterAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('id','name', 'active')
    list_editable = ['active']
    list_per_page = 20


def make_active(self, request, obj=None):
    obj.update(active=1)
make_active.short_description = "批量选择勾选的项"

def make_inactive(self, request, obj=None):
    obj.update(active=0)
make_inactive.short_description = "批量取消勾选的项"

class ProjectAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('id', 'name', 'code','active')
    list_editable = ['active']
    list_per_page = 20
    actions = [make_active, make_inactive]

class CpjfAdmin(admin.ModelAdmin):
    List_display_links = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(CpjfAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    list_filter = ('doing', 'active',)
    search_fields = ('project',)
    list_display = ('id','project', 'active','doing')
    list_editable = ['active']
    readonly_fields = ('project','doing')
    actions = [make_active,make_inactive]
    list_per_page = 20


admin.site.site_header = '后台管理系统'
admin.site.site_title = '后台管理系统'
admin.site.site_url = "/index"

admin.site.register(Project,ProjectAdmin)
admin.site.register(Issues,IssuesAdmin)
admin.site.register(Tasks,TasksAdmin)
admin.site.register(Reporter,ReporterAdmin)
admin.site.register(Cpjf,CpjfAdmin)