import pymysql
from django.shortcuts import render,redirect,reverse
from django.db import connection
import jira
from jira import JIRA
from . import config


def api_test(request):
    return render(request, 'api_test.html')


def jenkins(request):
    return render(request, 'jenkins.html')
