import yaml
# from . import config
import pymysql
import json
from . import config
host=config.host
user=config.user
password=config.password
db=config.db
port=config.port
jira_url=config.jira_url
basic_user=config.basic_user
basic_passwd=config.basic_passwd



def read_file(file_path):
    connection = pymysql.connect(host=host, user=user,
                                 password=password, db=db, port=port, charset='UTF8')
    cursor = connection.cursor()

    with open(file_path, encoding='UTF-8') as config_file:
        configs = yaml.safe_load(config_file)

    for path,info in configs['paths'].items():
        method=(list(info.keys())[0])
        rest_api={'url':path,'method':method}
        for k,v in list(info.values())[0].items():
            if k=='summary':
                rest_api.update({'summary': v.replace('\'','"')})
            if k == 'description':
                rest_api.update({'desc': v.replace('\'','"')})
            if k == 'tags':
                rest_api.update({'tag': ','.join(list(v))})
            if k=='operationId':
                rest_api.update({'oper_id': v})
            if k=='parameters':
                body_parameter=[]
                query_parameter=[]
                header_parameter=[]
                for i in v:
                    data = {'name': i.get('name').replace('\'','"'), 'required': i.get('required'), 'type': i.get('type','string')}
                    if i.get('in')=='body':
                        body_parameter.append(data)
                    elif i.get('in')=='header':
                        header_parameter.append(data)
                    else:
                        query_parameter.append(data)
                rest_api.update({'body': json.dumps(body_parameter).replace('\'','"'),
                                 'query':json.dumps(query_parameter).replace('\'','"'),
                                 'header': json.dumps(header_parameter).replace('\'', '"')
                                 })
            if k=='responses':
                responses={}
                for i,j in v.items():
                    responses.update({i:j.get('description')})
                rest_api.update({'response':json.dumps(responses).replace('\'','"')})

        cmd = "insert ignore into testcase_api values(NULL, '{5}','{6}','{7}','{8}','{0}_{1}','{0}','{1}','{2}','{3}','{4}' );".format(
            rest_api.get('url'),rest_api.get('method'),rest_api.get('body'),rest_api.get('query'),rest_api.get('response'),
            rest_api.get('tag'),rest_api.get('oper_id'),rest_api.get('summary'),rest_api.get('desc'))
        print(cmd)
        cursor.execute(cmd)
        connection.commit()
    connection.close()
# path=r'C:\Users\Administrator\Desktop\api.yaml'
# path=r'C:\Users\Administrator\Desktop\UYUN Openapi 索引表(CMDB) .yaml'
# read_file(path)
def make_case():
    connection = pymysql.connect(host=host, user=user,
                                 password=password, db=db, port=port, charset='UTF8')
    cursor = connection.cursor()
    cmd = "select url,method,query,body,response from testcase_api where id <43 ;"

    cursor.execute(cmd)
    res=cursor.fetchall()
    def make_value(json_data):
        if json_data['type']=='string':
            return {json_data['name']:''}
        elif json_data['type']=='integer':
            return {json_data['name']: 0}
        elif json_data['type']=='array':
            return {json_data['name']: []}
        elif json_data['type'] == 'boolean':
            return {json_data['name']: True}
    for i in res:
        query={}
        body={}
        if i[2] not in ('',None,'None'):
            datas=json.loads(i[2])
            for j in datas:
                query.update(make_value(j))
        if i[3] not in ('',None,'None'):
            datas = json.loads(i[3])
            for j in datas:
                query.update(make_value(j))
        if i[1]!='get':
            print(i[0],i[1])
            print(query)
            print (body)

def test():
    pass

make_case()