from django.urls import path

from . import views
from . import api

urlpatterns = [
        path('jenkins', views.jenkins, name='jenkins'),
        path('up_file', api.up_file, name='up_file'),
        # path('get_apis',api.get_apis,name='get_apis'),

        ]