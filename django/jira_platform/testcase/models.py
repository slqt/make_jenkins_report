from django.db import models


class Api(models.Model):
    class Meta:
        verbose_name_plural = '测试接口表'
    id=models.AutoField(primary_key = True)
    tag=models.CharField(max_length=50, verbose_name='接口类别', blank=True)
    version=models.CharField(max_length=50, verbose_name='接口版本', blank=True)
    oper_id=models.CharField(max_length=50, verbose_name='接口识别码', blank=True,db_index=True)
    summary=models.CharField(max_length=500, verbose_name='接口描述', blank=True)
    desc = models.CharField(max_length=999, verbose_name='接口详述', blank=True)
    url_method = models.CharField(max_length=100, verbose_name='请求地址和方法', blank=True, unique=True)
    url=models.CharField(max_length=100, verbose_name='请求地址', blank=True, db_index = True)
    method=models.CharField(max_length=20, verbose_name='请求方法', blank=True)
    body = models.CharField(max_length=999, verbose_name='请求体', blank=True)
    query = models.CharField(max_length=999, verbose_name='url信息', blank=True)
    header = models.CharField(max_length=999, verbose_name='header参数', blank=True)
    response = models.CharField(max_length=999, verbose_name='返回信息', blank=True)
    def __unicode__(self):
        return self.name


class Case(models.Model):
    class Meta:
        verbose_name_plural = '测试用例表'
    id=models.AutoField(primary_key = True)
    open_id=models.CharField(max_length=100, verbose_name='接口id', blank=True)
    name=models.CharField(max_length=100, verbose_name='用例名称', blank=True)
    desc=models.CharField(max_length=500, verbose_name='用例描述', blank=True)
    body = models.CharField(max_length=500, verbose_name='请求体', blank=True)
    query = models.CharField(max_length=500, verbose_name='query信息', blank=True)
    response = models.CharField(max_length=500, verbose_name='返回信息', blank=True)
    def __unicode__(self):
        return self.name


class Suit(models.Model):
    class Meta:
        verbose_name_plural = '测试场景表'
    id=models.AutoField(primary_key = True)
    name=models.CharField(max_length=100, verbose_name='场景名称', blank=True)
    cases=models.CharField(max_length=20, verbose_name='场景用例', blank=True)
    def __unicode__(self):
        return self.name

class Params(models.Model):
    class Meta:
        verbose_name_plural = '全局参数表'
    id=models.AutoField(primary_key = True)
    name=models.CharField(max_length=100, verbose_name='参数名称', blank=True)
    value = models.CharField(max_length=100, verbose_name='参数值', blank=True)
    des=models.CharField(max_length=20, verbose_name='描述', blank=True)
    def __unicode__(self):
        return self.name