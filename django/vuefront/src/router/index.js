import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import TestPlatform from '@/components/TestPlatform'
Vue.use(Router)

export default new Router({
  // mode: 'history',
  // 干掉地址栏里边的#号键
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/test_api',
      name: 'TestPlatform',
      component: TestPlatform
    }
  ]
})
