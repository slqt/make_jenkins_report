//项目面板页面展现
layui.use('table', function(){
  var table = layui.table;
var $ = layui.$,

search_issue = {
    reload: function(){
      var demoReload = $('#demoReload');
      table.reload('table', {
        page: {
          curr: 1
        }
        ,where: {
          key: {
            id: demoReload.val()
          }
        }
      });
    }
  };

change_table = {
    reload: function(){
        //获取总单状态复选框值
            var data='';
            $('input[name="status"]:checked').each(function(){
            data += $(this).val()+',';
            });
        var data1='';
        $("input:checkbox[name='onoffswitch']:checked").each(function() {
            data1 = ',' + $(this).val();
        });
      table.reload('table', {
        page: {
          curr: 1
        }
        ,where: {
          key: {
            status: data,
            sumdata: data1
          }
        }
      });
    }
  };

   $('#onoffswitch').on('click', function(){
        var type = "reload";
        change_table[type] ? change_table[type].call(this) : '';
    });
       $('#getStatus').on('click', function(){
        var type = "reload";
        change_table[type] ? change_table[type].call(this) : '';
    });

  $('#bar .layui-btn').on('click', function(){
    var type = $(this).data('type');
    search_issue[type] ? search_issue[type].call(this) : '';
  });

  table.on('tool(table)', function(obj){
    var datas = obj.data
    ,layEvent = obj.event;
    if(layEvent === 'all' || '10' ){
    var proup = $('#branch .branch-proup');
        datas['type']=layEvent;
         $.ajax({
              type: "POST",
              url: "/index/get_dashboard_echarts",
              dataType: "json",
              data:datas,
              success : function(data){
                console.log(data);
                var myChartObj=document.getElementById('container')
                var resizeWorldMapContainer = function () {
                    myChartObj.style.width = window.innerWidth*0.8+ 'px';
                    myChartObj.style.height = window.innerHeight*0.6 + 'px';
                };
                resizeWorldMapContainer();
                var myChart = echarts.init(myChartObj);

                var options = {
                    color:['#9BCD9B','lightblue','orange','red'],
                    title : {
                        text: data.title,
                        x:'center'
                    },
                    backgroundColor: 'white',
                    legend: {
                            height: myChartObj.style.height,
                            width: myChartObj.style.width,
                             data:['bug总数','reopen数目','提测失败','未解决'],
                             x : 'center',
                                y : 'bottom',
                                selected:{'reopen数目':false,
                                '提测失败':false,
                                 '未解决':false
                                }
                            },
                    xAxis: [
                          {
                            type : 'category',
                            data: data.xaxis
                        },
                        {
                            gridIndex: 1,
                            type : 'category',
                            data:  data.xaxis,
                        }
                    ],
                     tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            animation: false
                        }
                    },
                    yAxis: [
                            {
                            name : '当天bug个数',
                            type : 'value',
                        },
                        {
                            gridIndex: 1,
                            name : '累计bug个数',
                            type : 'value',
                        }
                    ],
                    grid: [
                        {bottom: '55%'},
                        {top: '55%'}
                    ],
                    series: [
                     {
                        name:'bug总数',
                        type:'bar',
                         barWidth : 10,
                        data:data.total_bugs
                    },
                     {
                        name:'reopen数目',
                        type:'bar',
                         barWidth : 10,
                        data: data.reopens
                    },
                    {
                        name:'未解决',
                        type:'bar',
                         barWidth : 10,
                        data: data.unsolves
                    },
                    {
                        name:'提测失败',
                        type:'bar',
                         barWidth : 10,
                        data: data.fails
                    },
                     {
                        name:'bug总数',
                        type:'bar',
                         barWidth : 10,
                        xAxisIndex: 1,
                        yAxisIndex: 1,
                        data:data.total_bugs_all
                    },
                     {
                        name:'reopen数目',
                        type:'bar',
                        xAxisIndex: 1,
                        yAxisIndex: 1,
                         barWidth : 10,
                        data: data.reopens_all
                    },
                    {
                        name:'未解决',
                        type:'bar',
                        xAxisIndex: 1,
                        yAxisIndex: 1,
                         barWidth : 10,
                        data: data.unsolves_all
                    },
                    {
                        name:'提测失败',
                        type:'bar',
                        xAxisIndex: 1,
                        yAxisIndex: 1,
                         barWidth : 10,
                        data: data.fails_all
                    }
                    ]
                }

                if (options && typeof options === "object") {
                    myChart.setOption(options, true);
                }
                window.onresize = function(){
                myChart.resize();
                }
                proup.show();
                },
                    error:function(){
                    alert("请求失败，请勿频繁点击");
                    console.log(s);
                    s='';
                    cancle();
                }
          }),
          proup.on('click',function (e) {
                if($(e.target).closest('#branch .container').length <= 0){
                    proup.hide()
                }
            });
    }
  });

});