//cpjf表格展现
layui.use('table', function(){
  var table = layui.table;
var $ = layui.$,
active = {
    reload: function(){
      var demoReload = $('#demoReload');
      table.reload('table', {
        page: {
          curr: 1
        }
        ,where: {
          key: {
            id: demoReload.val()
          }
        }
      });
    }
  };

active1 = {
    reload: function(){
      var s='';
        $("input:checkbox[name='status']:checked").each(function() {
            s += ',' + $(this).val();
        });
      table.reload('table', {
        page: {
          curr: 1
        }
        ,where: {
          key: {
            status: s
          }
        }
      });
    }
  };

  $('#getStatus').on('click', function(){
        var s='';
        $("input:checkbox[name='status']:checked").each(function() {
            s += ',' + $(this).val();
        });
        var type = $(this).data('type');
        active1[type] ? active1[type].call(this) : '';
    });
});