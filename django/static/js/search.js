//搜索按钮

 layui.use('table', function(){
      var table = layui.table;
    var $ = layui.$,
    active = {
        reload: function(){
          var tableReload = $('#tableReload');

          table.reload('table', {
            page: {
              curr: 1
            }
            ,where: {
              key: {
                search_content: $("input[name=search_content]").val()
              }
            }
          });
        }
      };

      $('.searchBtn .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
      });
      $('.searchBtn .layui-input').on('keydown',function(e){
        if(e.keyCode==13){
             var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        }
        });
    });